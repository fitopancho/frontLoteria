
<p></p><h1>APP-Loteria</h1> <h3>Android</h3></p><h5>1.0.0</h5>

## ¿Qué es?

<p>Esta APP-Loteria es una pequeña APP con fines academicos, para poner en practica conocimientos adquiridos en el tiempo. Se alimenta de https://gitlab.com/fitopancho/APILoteria una API tambien creada por mi. Esta APP se encarga de mostrar los datos de la API de manera más estilizada. </p>

<p> Versión final de la aplicacion. Se deben agregar más validaciones para mejor UI/UX, pero está funcional</p>
![menu](https://fitopanchodev.cl/laravel8/public/images/Screenshot_1693258198.png)
![ultimo sorteo](https://fitopanchodev.cl/laravel8/public/images/Screenshot_1693258194.png)


## AMBIENTE

- Node v18.16.0
- react-native 0.72.4
- react 18.2.0


