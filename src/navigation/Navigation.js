import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import { HomeScreen } from '../screens/HomeScreen';
import { ResultScreen } from '../screens/ResultScreen';
import { PronosticoScreen } from '../screens/PronosticoScreen';
import { ConsultaScreen } from '../screens/ConsultaScreen'


const Stack = createStackNavigator();

export const Navigation = () => {
  return (
    <Stack.Navigator 
        screenOptions = {{
            headerShown: false,
            cardStyle: {
                backgroundColor: 'white'
            }
        }}
    >
      <Stack.Screen name="HomeScreen"  component={HomeScreen} />
      <Stack.Screen name="ResultScreen"  component={ResultScreen} />
      <Stack.Screen name="PronosticoScreen"  component={PronosticoScreen} />
      <Stack.Screen name="ConsultaScreen"  component={ConsultaScreen} />
    </Stack.Navigator>
  );
}