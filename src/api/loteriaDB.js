import axios from 'axios';

const loteriaDB = axios.create({
    baseURL: 'http://127.0.0.1:8000/api/sorteo'
});

export default loteriaDB;