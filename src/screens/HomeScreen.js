
import { useState } from 'react'
import {  StyleSheet,Text, View, Image, Alert, TouchableOpacity, ActivityIndicator  } from 'react-native'
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import loteriaDB from "../api/loteriaDB";
import LinearGradient from 'react-native-linear-gradient';
import { useNavigation } from '@react-navigation/core';


export const HomeScreen = () => {
    const {top} = useSafeAreaInsets();
    TouchableOpacity.defaultProps = { activeOpacity: 0.8 };
    const navigation = useNavigation();
    const [isLoading, setIsLoading] = useState(false)

    const actualizarData = async () =>{
        try{
            setIsLoading(true)
            const request = loteriaDB.get('/actualizar');
            const response = await Promise.all([
                request
            ])
            console.log(response[0].data)
            if(response[0].data.cd_msg == 0){
                Alert.alert('Datos actualizados correctamente')
            }else{
                Alert.alert('Hubo un problema al actualizar los datos')
            }
            setIsLoading(false)
            
            
        }catch(error){
            setIsLoading(false)
            Alert.alert('Hubo un problema al actualizar los datos')
            console.log(error)
        }
    }

    return (
    <View style={styles.container}>
        <View style={{marginTop: top+25}}>
            <View style={styles.container_name}>
                
                <Image
                    style={styles.tinyLogo}
                    source={{
                        uri: 'https://www.loteria.cl/loterianet/Content/img/menu/loteria.png',
                        }}
                />
            </View>
            <View style={styles.container_buttons}>
                <TouchableOpacity
                    onPress={() => actualizarData()}//debe llamar una funcion aqui mismo y luego lanzar una alerta de OK
                    style={styles.buttons}
                >
                    <LinearGradient
                        colors={["#0081C5", "#66D3F0"]}
                        style={styles.appButtonContainer}
                    >
                        <Text style={styles.text_Button}>Actualizar datos</Text>
                    </LinearGradient>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => navigation.navigate('ConsultaScreen', ["Consultar sorteo", ""])}
                    style={styles.buttons}
                >
                    <LinearGradient
                        colors={["#0081C5", "#66D3F0"]}
                        style={styles.appButtonContainer}
                    >
                        <Text style={styles.text_Button}>Consultar sorteo</Text>
                    </LinearGradient>
                </TouchableOpacity>
            </View>
            <View style={styles.container_buttons}>
                <TouchableOpacity
                    onPress={() => navigation.navigate('ResultScreen', ["Último sorteo", "ultimo"])} 
                    
                    style={styles.buttons}
                >
                    <LinearGradient
                        colors={["#0081C5", "#66D3F0"]}
                        style={styles.appButtonContainer}
                    >
                        <Text style={styles.text_Button}>Consultar último sorteo</Text>
                    </LinearGradient>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => navigation.navigate('PronosticoScreen', ["Pronostico", "pronostico"])}
                    style={styles.buttons}
                >
                    <LinearGradient
                        colors={["#0081C5", "#66D3F0"]}
                        style={styles.appButtonContainer}
                    >
                        <Text style={styles.text_Button}>Consultar pronóstico</Text> 
                    </LinearGradient> 
                </TouchableOpacity>
            </View>
        </View>
        {
            isLoading
            ?<ActivityIndicator size={35} color="white" style={{marginTop: 20}}/>
            :""
        }
        
        
        
    </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: '#0069B7'
    },

    container_name: {
        padding: 10,
        alignItems: 'center',   
    },
    name:{
        textAlign: 'center',
        fontSize: 50,
        fontWeight: 'bold'
    },
    container_buttons: {
        padding: 10,
        flexDirection: 'row',
    },
    buttons:{
        alignItems: 'center',
        padding: 10,
        margin: 10,
    },
    appButtonContainer: {
        elevation: 8,
        borderRadius: 10,
        paddingVertical: 15,
        paddingHorizontal: 12
      },
    tinyLogo: {
        
        width: 100,
        height: 100,
      },
    text_Button:{
        textAlign: 'center',
        textAlignVertical:'center',
        width: 100,
        height: 100,
        color: 'white',
        fontWeight: 'bold',
        fontSize:20
        
      }
  });
