
import { useEffect, useState } from 'react'
import {  StyleSheet,Text, View, TouchableOpacity ,ActivityIndicator } from 'react-native'
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import loteriaDB from "../api/loteriaDB";
import LinearGradient from 'react-native-linear-gradient';


export const PronosticoScreen = (route, navigation) => {
    const {top} = useSafeAreaInsets();
    const url = route.route.params[1];

    TouchableOpacity.defaultProps = { activeOpacity: 0.8 };

    const [respPronostico, setRespPronostico] = useState({})
    const [isLoading, setIsLoading] = useState(true)
    useEffect(() => {
      
      getResultados();
       
     }, [])

    const getResultados = async () =>{
      try{
        const request = loteriaDB.get('/'+url);
        const response = await Promise.all([
          request
        ])
        setRespPronostico(response[0].data.numeros)
        setIsLoading(false)
      }catch(error){
        console.log("fallamos")
        console.log(error)
      }
        
    }
    
    
  return (
    <View style={styles.container}>
      <View style={{marginTop: top+25}}>
        <View style={styles.container_name}><Text style={styles.name}>{route.route.params[0]}</Text></View>
        
      </View>
      {
        isLoading
          ?<ActivityIndicator size={35} color="white" style={{marginTop: 20}}/>
          :(<View style={styles.await_contenedor}>
            <View style={styles.container_text}>
              <Text style={styles.text}>Pronostico proximo sorteo</Text>
            </View>
            <View style={styles.num_container}>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respPronostico[0].numero}</Text>
              </LinearGradient>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respPronostico[1].numero}</Text>
              </LinearGradient>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respPronostico[2].numero}</Text>
              </LinearGradient>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respPronostico[3].numero}</Text>
              </LinearGradient>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respPronostico[4].numero}</Text>
              </LinearGradient>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respPronostico[5].numero}</Text>
              </LinearGradient>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respPronostico[6].numero}</Text>
              </LinearGradient>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respPronostico[7].numero}</Text>
              </LinearGradient>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respPronostico[8].numero}</Text>
              </LinearGradient>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respPronostico[9].numero}</Text>
              </LinearGradient>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respPronostico[10].numero}</Text>
              </LinearGradient>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respPronostico[11].numero}</Text>
              </LinearGradient>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respPronostico[12].numero}</Text>
              </LinearGradient>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respPronostico[13].numero}</Text>
              </LinearGradient>
    
    
            </View>
          </View>)
          
      }
        
      
      
      
        
    </View>
  )
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: '#0069B7'
  },

  container_name: {
    padding: 10,
    alignItems: 'center',   
  },
  name:{
    textAlign: 'center',
    fontSize: 50,
    fontWeight: 'bold',
    color: "white"
  },
  container_text: {
    padding: 10,
    flexDirection: 'row',
  },
  text:{
    color:"white",
    fontWeight: 'bold'
  },
  num_container:{
    flex: 1,
    justifyContent:"center",
    flexDirection: 'row',
    flexWrap: 'wrap',
    width:"50%",
  },
  bolita:{
    width: 40,
    height: 40,
    borderRadius: 25,
    alignItems: 'center',
    margin: 2
  },
  numero:{
    marginTop: 10,
    fontWeight: 'bold',
    color: "black"
  },
  await_contenedor:{
    
  }
  });
