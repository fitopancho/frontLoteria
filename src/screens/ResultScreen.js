
import { useEffect, useState } from 'react'
import {  StyleSheet,Text, View, TouchableOpacity ,ActivityIndicator } from 'react-native'
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import loteriaDB from "../api/loteriaDB";
import LinearGradient from 'react-native-linear-gradient';


export const ResultScreen = (route, navigation) => {
    const {top} = useSafeAreaInsets();
    const url = route.route.params[1];

    TouchableOpacity.defaultProps = { activeOpacity: 0.8 };

    const [respUltimoSorteo, setRespUltimoSorteo] = useState({})
    const [isLoading, setIsLoading] = useState(true)
    useEffect(() => {
      
      getResultados();
       
     }, [])

    const getResultados = async () =>{
      try{
        const request = loteriaDB.get('/'+url);
        const response = await Promise.all([
          request
        ])
        setRespUltimoSorteo(response[0].data.sorteo)
        setIsLoading(false)
      }catch(error){
        console.log("fallamos")
        console.log(error)
      }
        
    }
    
    
  return (
    <View style={styles.container}>
      <View style={{marginTop: top+25}}>
        <View style={styles.container_name}><Text style={styles.name}>{route.route.params[0]}</Text></View>
        
      </View>
      {
        isLoading
          ?<ActivityIndicator size={35} color="white" style={{marginTop: 20}}/>
          :(<View style={styles.await_contenedor}>
            <View style={styles.container_text}>
              <Text style={styles.text}>{"n° sorteo: "+respUltimoSorteo.n_sorteo+"  -  "}</Text><Text style={styles.text}>{respUltimoSorteo.fecha_sorteo}</Text>
            </View>
            <View style={styles.num_container}>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respUltimoSorteo.num1}</Text>
              </LinearGradient>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respUltimoSorteo.num2}</Text>
              </LinearGradient>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respUltimoSorteo.num3}</Text>
              </LinearGradient>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respUltimoSorteo.num4}</Text>
              </LinearGradient>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respUltimoSorteo.num5}</Text>
              </LinearGradient>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respUltimoSorteo.num6}</Text>
              </LinearGradient>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respUltimoSorteo.num7}</Text>
              </LinearGradient>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respUltimoSorteo.num8}</Text>
              </LinearGradient>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respUltimoSorteo.num9}</Text>
              </LinearGradient>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respUltimoSorteo.num10}</Text>
              </LinearGradient>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respUltimoSorteo.num11}</Text>
              </LinearGradient>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respUltimoSorteo.num12}</Text>
              </LinearGradient>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respUltimoSorteo.num13}</Text>
              </LinearGradient>
              <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                <Text style={styles.numero}>{respUltimoSorteo.num14}</Text>
              </LinearGradient>
    
    
            </View>
          </View>)
          
      }
        
      
      
      
        
    </View>
  )
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: '#0069B7'
  },

  container_name: {
    padding: 10,
    alignItems: 'center',   
  },
  name:{
    textAlign: 'center',
    fontSize: 50,
    fontWeight: 'bold',
    color: "white"
  },
  container_text: {
    padding: 10,
    flexDirection: 'row',
  },
  text:{
    color:"white",
    fontWeight: 'bold'
  },
  num_container:{
    flex: 1,
    justifyContent:"center",
    flexDirection: 'row',
    flexWrap: 'wrap',
    width:"50%",
  },
  bolita:{
    width: 40,
    height: 40,
    borderRadius: 25,
    alignItems: 'center',
    margin: 2
  },
  numero:{
    marginTop: 10,
    fontWeight: 'bold',
    color: "black"
  },
  await_contenedor:{
    
  }
  });
