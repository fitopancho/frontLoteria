
import { useEffect, useState } from 'react'
import {  StyleSheet,Text, View, TouchableOpacity ,ActivityIndicator, TextInput, Alert } from 'react-native'
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import loteriaDB from "../api/loteriaDB";
import LinearGradient from 'react-native-linear-gradient';


export const ConsultaScreen = (route, navigation) => {
    const {top} = useSafeAreaInsets();
    const url = route.route.params[1];

    TouchableOpacity.defaultProps = { activeOpacity: 0.8 };

    const [number, onChangeNumber] = useState('');
    const [isLoading, setIsLoading] = useState(true)
    const [cargaPosterior, setCargaPosterior] = useState(false)
    const [resp, setResp] = useState()


    const getResultados = async () =>{
        setCargaPosterior(true)
      try{
        setIsLoading(true)
        const request = loteriaDB.get('/id/'+number);
        const response = await Promise.all([
          request
        ])
        console.log(response[0].data)
        if(response[0].data.cd_msg == 0){
            setResp(response[0].data.sorteo)
            setIsLoading(false)
        }
        
      }catch(error){
        console.log("fallamos")
        console.log(error)
      }
        
    }
    
    
  return (
    <View style={styles.container}>
      <View style={{marginTop: top+25}}>
        <View style={styles.container_name}><Text style={styles.name}>{route.route.params[0]}</Text></View>
        
      </View>
      
        <View style={styles.await_contenedor}>
            <View style={styles.container_text}>
                <TextInput
                    style={styles.input}
                    onChangeText={onChangeNumber}
                    value={number}
                    placeholder="Ingrese el número de sorteo"
                    keyboardType="numeric"
                    clearTextOnFocus ={true}
                />
                <TouchableOpacity
                    onPress={() => getResultados()}
                    style={styles.buttons}
                >
                    <LinearGradient
                        colors={["#0081C5", "#66D3F0"]}
                        style={styles.appButtonContainer}
                    >
                        <Text style={styles.text_Button}>Buscar</Text>
                    </LinearGradient>
                </TouchableOpacity>
            </View>   
        </View>
        {
            cargaPosterior?
                isLoading
                    ?<ActivityIndicator size={35} color="white" style={{marginTop: 20}}/>
                    :(<View style={styles.await_contenedor}>
                        <View style={styles.container_text}>
                        <Text style={styles.text}>Sorteo n°{resp.n_sorteo} - {resp.fecha_sorteo}</Text>
                        </View>
                        <View style={styles.num_container}>
                        <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                            <Text style={styles.numero}>{resp.num1}</Text>
                        </LinearGradient>
                        <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                            <Text style={styles.numero}>{resp.num2}</Text>
                        </LinearGradient>
                        <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                            <Text style={styles.numero}>{resp.num3}</Text>
                        </LinearGradient>
                        <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                            <Text style={styles.numero}>{resp.num4}</Text>
                        </LinearGradient>
                        <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                            <Text style={styles.numero}>{resp.num5}</Text>
                        </LinearGradient>
                        <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                            <Text style={styles.numero}>{resp.num6}</Text>
                        </LinearGradient>
                        <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                            <Text style={styles.numero}>{resp.num7}</Text>
                        </LinearGradient>
                        <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                            <Text style={styles.numero}>{resp.num8}</Text>
                        </LinearGradient>
                        <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                            <Text style={styles.numero}>{resp.num9}</Text>
                        </LinearGradient>
                        <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                            <Text style={styles.numero}>{resp.num10}</Text>
                        </LinearGradient>
                        <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                            <Text style={styles.numero}>{resp.num11}</Text>
                        </LinearGradient>
                        <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                            <Text style={styles.numero}>{resp.num12}</Text>
                        </LinearGradient>
                        <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                            <Text style={styles.numero}>{resp.num13}</Text>
                        </LinearGradient>
                        <LinearGradient style={styles.bolita} colors={["#EBBC3B", "#EB8661"]}>
                            <Text style={styles.numero}>{resp.num14}</Text>
                        </LinearGradient>
                
                
                        </View>
                    </View>)
                    :''
          
      }
    </View>
  )
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: '#0069B7'
  },

  container_name: {
    padding: 10,
    alignItems: 'center',   
  },
  name:{
    textAlign: 'center',
    fontSize: 50,
    fontWeight: 'bold',
    color: "white"
  },
  container_text: {
    padding: 10,
    flexDirection: 'row',
  },
  text:{
    color:"white",
    fontWeight: 'bold'
  },
  num_container:{
    flex: 1,
    justifyContent:"center",
    flexDirection: 'row',
    flexWrap: 'wrap',
    width:"50%",
  },
  bolita:{
    width: 40,
    height: 40,
    borderRadius: 25,
    alignItems: 'center',
    margin: 2
  },
  numero:{
    marginTop: 10,
    fontWeight: 'bold',
    color: "black"
  },
  await_contenedor:{
    
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 0,
    padding: 10,
    borderRadius: 25,
    backgroundColor: "white"
  },
  buttons:{
    alignItems: 'center',
    padding: 0,
    margin: 10,
  },
  appButtonContainer: {
    elevation: 8,
    borderRadius: 25,
    paddingVertical: 10,
    paddingHorizontal: 12
  },
  });
 